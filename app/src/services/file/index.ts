import { deleteFile } from './delete';
import { downloadFile } from './download';
import { previewFile } from './preview';
import { receiveFileInfo } from './receive';
import { shareFile } from './share';
import { uploadFile } from './upload';

export {
  deleteFile,
  downloadFile,
  previewFile,
  receiveFileInfo,
  shareFile,
  uploadFile,
};
