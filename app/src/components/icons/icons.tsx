import { ReactComponent as ChevronDown } from 'src/media/UI/chevron_down.svg';
import { ReactComponent as Spinner } from 'src/media/UI/spinner.svg';
import { ReactComponent as Success } from 'src/media/UI/check_circle_regular.svg';
import { ReactComponent as Fail } from 'src/media/UI/times_circle_solid.svg';
import { ReactComponent as InfoIcon } from 'src/media/UI/info.svg';
import { ReactComponent as NavRight } from 'src/media/UI/chevron_breadcrumb.svg';
import { ReactComponent as QuestionMark } from 'src/media/UI/question.svg';
import { ReactComponent as Switch } from 'src/media/UI/switch.svg';
import { ReactComponent as Search } from 'src/media/UI/search.svg';
import { ReactComponent as Folder } from 'src/media/UI/folder.svg';
import { ReactComponent as Upload } from 'src/media/UI/upload.svg';
import { ReactComponent as Plus } from 'src/media/UI/plus.svg';
import { ReactComponent as Bell } from 'src/media/fairdrive/Bell.svg';
import { ReactComponent as Dashboard } from 'src/media/fairdrive/Dashboard.svg';
import { ReactComponent as Drive } from 'src/media/fairdrive/Drive.svg';
import { ReactComponent as Filter } from 'src/media/fairdrive/Filter.svg';
import { ReactComponent as Globe } from 'src/media/fairdrive/Globe.svg';
import { ReactComponent as Image } from 'src/media/fairdrive/Image.svg';
import { ReactComponent as Profile } from 'src/media/fairdrive/Profile.svg';
import { ReactComponent as SearchFD } from 'src/media/fairdrive/Search.svg';
import { ReactComponent as Settings } from 'src/media/fairdrive/Settings.svg';
import { ReactComponent as Time } from 'src/media/fairdrive/Time.svg';
import { ReactComponent as Logo } from 'src/media/fairdrive/logo.svg';
import { ReactComponent as File } from 'src/media/fileTypes/File.svg';
import { ReactComponent as Directory } from 'src/media/fileTypes/Directory.svg';
import { ReactComponent as ChevronRight } from '../../media/UI/chevron_right.svg';
import { ReactComponent as ArrowRight } from 'src/media/fairdrive/ArrowRight.svg';
import { ReactComponent as SearchLoupe } from 'src/media/fairdrive/SearchLoupe.svg';
import { ReactComponent as StartFolder } from 'src/media/UI/getStartedFolder.svg';
import { ReactComponent as Dapps } from 'src/media/UI/dapps.svg';
import { ReactComponent as Complete } from 'src/media/UI/complete.svg';
import { ReactComponent as Close } from 'src/media/UI/close.svg';
import { ReactComponent as Kebab } from 'src/media/UI/kebab.svg';
import { ReactComponent as PodChevron } from 'src/media/UI/pod_chevron.svg';
import { ReactComponent as PodInfo } from 'src/media/UI/pod_info.svg';
import { ReactComponent as QuestionCircle } from 'src/media/UI/question_small.svg';
import { ReactComponent as Chevron } from 'src/media/UI/chevron.svg';
import { ReactComponent as Download } from 'src/media/UI/download.svg';
import { ReactComponent as Hide } from 'src/media/UI/hide.svg';
import { ReactComponent as Share } from 'src/media/UI/share.svg';
import { ReactComponent as UploadIcon } from 'src/media/UI/upload-new.svg';
import { ReactComponent as FilterIcon } from 'src/media/optionIcons/Filter.svg';
import { ReactComponent as SortingIcon } from 'src/media/optionIcons/SortingIcon.svg';
import { ReactComponent as GridIcon } from 'src/media/optionIcons/GridIcon.svg';
import { ReactComponent as ListIcon } from 'src/media/optionIcons/ListIcon.svg';
import { ReactComponent as DAppIcon } from 'src/media/UI/dapp_square.svg';
import { ReactComponent as ShareIcon } from 'src/media/UI/share_icon.svg';
import { ReactComponent as ButtonPlus } from 'src/media/UI/button_plus.svg';
import { ReactComponent as ModalFolder } from 'src/media/UI/modalfolder.svg';
import { ReactComponent as Copy } from 'src/media/UI/copy.svg';
import { ReactComponent as Github } from 'src/media/socialMedia/github.svg';
import { ReactComponent as Medium } from 'src/media/socialMedia/medium.svg';
import { ReactComponent as Discord } from 'src/media/socialMedia/discord.svg';
import { ReactComponent as Warning } from 'src/media/optionIcons/warning.svg';
import { ReactComponent as CirclePart } from 'src/media/spinners/circle_part.svg';

import { ReactComponent as Swarm } from 'src/media/logotypes/Swarm.svg';
import { ReactComponent as LinumLabs } from 'src/media/logotypes/LinumLabs.svg';
import { ReactComponent as FairDataSociety } from 'src/media/logotypes/FairDataSociety.svg';
import { ReactComponent as TailSpinner } from 'src/media/fairdrive/TailSpinner.svg';
import { ReactComponent as Trash } from 'src/media/fairdrive/Trash.svg';
import { ReactComponent as Moon } from 'src/media/optionIcons/Moon.svg';
import { ReactComponent as Sun } from 'src/media/optionIcons/Sun.svg';
export const icons = {
  QuestionMark: QuestionMark,
  NavRight: NavRight,
};

export {
  Trash,
  TailSpinner,
  ButtonPlus,
  ShareIcon,
  UploadIcon,
  PodInfo,
  Hide,
  Share,
  Download,
  PodChevron,
  StartFolder,
  Spinner,
  Success,
  Fail,
  Folder,
  SearchLoupe,
  InfoIcon,
  NavRight,
  QuestionMark,
  Switch,
  Search,
  Upload,
  Plus,
  Bell,
  Dashboard,
  Drive,
  Filter,
  Globe,
  Image,
  Profile,
  SearchFD,
  Settings,
  Time,
  Dapps,
  Complete,
  Kebab,
  Close,
  QuestionCircle,
  Chevron,
  ModalFolder,
  Copy,
  FilterIcon,
  SortingIcon,
  ListIcon,
  GridIcon,
  DAppIcon,
  Logo,
  LinumLabs,
  Warning,
  FairDataSociety,
  Github,
  Medium,
  Discord,
  Moon,
  Sun,
  CirclePart,
  Swarm,
  File,
  Directory,
  ChevronDown,
  ChevronRight,
  ArrowRight,
};
