export type FilePreviewInfo = {
  filename: string;
  directory: string;
  podName: string;
};
