export enum AVAILABLE_PAGES {
  OVERVIEW = 'Overview',
  DRIVE = 'Drive',
  EXPLORE = 'Explore',
}
