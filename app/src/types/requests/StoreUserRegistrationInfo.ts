export interface IStoreUserRegistrationInfo {
  username: string;
  password: string;
  inviteCode: string;
}
