export interface IGetDirectory {
  directory: string;
  password?: string;
  podName: string;
}
