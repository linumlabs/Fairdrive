export interface IUserLogin {
  username: string;
  password: string;
  podName?: string;
}
