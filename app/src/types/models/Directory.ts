export interface IDirectory {
  name: string;
  content_type?: string;
  creation_time?: string;
  modification_time?: string;
  access_time?: string;
  size?: string;
  block_size?: string;
}
