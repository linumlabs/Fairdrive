export interface CreateAccount {
  username: string;
  password: string;
  mnemonic: string;
}
