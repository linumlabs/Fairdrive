export interface IUserStats {
  user_name: string;
  reference: string;
  avatar: string;
}
