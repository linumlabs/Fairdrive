export interface IFile {
  name: string;
  content_type: string;
  size: string;
  block_size: string;
  creation_time: string;
  modification_time: string;
  access_time: string;
}
